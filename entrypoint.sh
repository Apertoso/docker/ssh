#!/bin/sh

if [ $(grep -ci ${SSH_USER_NAME} /etc/shadow) -eq 0 ]; then
    useradd ${SSH_USER_NAME} -d ${SSH_USER_HOME} --no-create-home
fi
if [ ! -z ${SSH_USER_PASSWORD} ]; then
    echo ${SSH_USER_NAME}:${SSH_USER_PASSWORD} | chpasswd
fi
chown -R ${SSH_USER_NAME}:${SSH_USER_NAME} ${SSH_USER_HOME}

if [ ! -f /etc/ssh/sshd_config ]; then
    cp -av /etc-ssh/* /etc/ssh/
fi

exec /usr/sbin/sshd -D -e
