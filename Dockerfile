FROM debian:stretch-slim

RUN set -x \
    # Update APT cache & install upgrades
    && apt-get update \
    && apt-get upgrade -y \

    # install openssh server
    && apt-get install -y --no-install-recommends \
        openssh-server \
        autossh \
        net-tools \
    && mkdir /var/run/sshd \

    # setup config
    && sed -i 's/.*PermitRootLogin .*/PermitRootLogin yes/' /etc/ssh/sshd_config \
    && sed -i 's/.*#GatewayPorts .*/GatewayPorts yes/'  /etc/ssh/sshd_config \
    && echo 'ClientAliveInterval 10'  >> /etc/ssh/sshd_config  \
    && echo 'ClientAliveCountMax 10'  >> /etc/ssh/sshd_config  \
    && sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd \
    && mv /etc/ssh /etc-ssh/ \

    # Do cleanup
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \

    && echo "Ok"


EXPOSE 22
EXPOSE 9100-9109

VOLUME /etc/ssh

COPY entrypoint.sh /

CMD ["/entrypoint.sh"]

